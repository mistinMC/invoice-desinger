<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class invInvoiceController extends Controller
{
    //
    protected $invoice;
    protected $blockList;
    protected $json;
    protected $list;

    public function __construct()
    {
        $this->blockList= ['index','detail'];
        $this->invoice = [];
        $file = file_get_contents('https://uygulama.nkolayofis.com/test/490');
        $this->json = $file;
        $this->list = ['genel-toplam','ara-toplam','kdv'];

    }

    public function preview($id,$fatura_id=false){


        $this->setId($id);
        $menu_items = $this->getMenu();
        $data = DB::table('inv_invoices')->where('id',$id)->first();
        if(!$data){
            return view('errors.404');
        }
        $items = $this->listItemsWithArray($id);
        return view('inv/preview',['id'=>$id,'menu_items'=>$menu_items,'data'=>$data,'items'=>$items]);

    }
    public function index(){

        $menu_items = $this->getMenu();

        return view('inv/index',['menu_items'=>$menu_items]);
    }

    public function detail($id){

        $this->setId($id);
        $menu_items = $this->getMenu();
        $data = DB::table('inv_invoices')->where('id',$id)->first();
        if(!$data){
            return view('errors.404');
        }

        $items = $this->listItemsWithArray($id);

        return view('inv/edit',['id'=>$id,'menu_items'=>$menu_items,'data'=>$data,'items'=>$items]);
    }

    public function api($method,Request $request){

        $id = $request->header('INVOICE-ID');

        $this->setId($id);
        $this->setData($this->getId());

        if(!method_exists($this,$method)){
            return 'method not found';
        }
        if(in_array($method,$this->blockList)){
            return 'Permission denied !';
        }

        return call_user_func_array(array($this, $method), [$request]);
    }

    public  function callAPI(){
        $file = $this->json;
        return response()->json(json_decode($file),200);
    }
    protected   function addTableWithArray($input){

        $array = [$input['id'],$input['text'],$input['attr'],$input['w'],$input['h'],$input['l'],$input['t']];

        list($id,$text,$attr,$w,$h,$l,$t)  = $array;

        $itemsCoordinate = $this->setCoordinate($id,$w,$h,$l,$t);

        $item = [
            'id' => $id,
            'text' => $text,
            'attr' => $attr,
            'type' => 'txt',
            'style' => $this->getCoordinates($itemsCoordinate),
            'coordinates'=>$itemsCoordinate
        ];

        $cols = $input['cols'];
        $data = $input['elements'];

        return $this->getTable($item ,$cols,$data);
    }
    protected  function addTable($request){

        $input = $request->all();
        $array = [$input['id'],$input['text'],$input['attr'],$input['w'],$input['h'],$input['l'],$input['t']];

            list($id,$text,$attr,$w,$h,$l,$t)  = $array;

            $itemsCoordinate = $this->setCoordinate($id,$w,$h,$l,$t);

            $item = [
                'id' => $id,
                'text' => $text,
                'attr' => $attr,
                'type' => 'txt',
                'style' => $this->getCoordinates($itemsCoordinate),
                'coordinates'=>$itemsCoordinate
            ];

        $cols = $input['cols'];
        $data = $input['elements'];

            return $this->getTable($item ,$cols,$data);


    }
    protected function addItem($request){
        $input = $request->all();
        $array = [$input['id'],$input['text'],$input['attr'],$input['w'],$input['h'],$input['l'],$input['t']];
        list($id,$text,$attr,$w,$h,$l,$t)  = $array;

        return $this->setItem($id,$text,$attr,$w,$h,$l,$t);
    }

    protected function setItem($id,$text,$attr,$w,$h,$l,$t){



            $itemsCoordinate = $this->setCoordinate($id,$w,$h,$l,$t);

            $item = [
                'id' => $id,
                'text' => $text,
                'attr' => $attr,
                'type' => 'txt',
                'style' => $this->getCoordinates($itemsCoordinate),
                'coordinates'=>$itemsCoordinate
            ];

                $html = $this->getList($item);



            return  response()->json($html);

    }
    protected function setCoordinate($id,$w,$h,$l,$t){


        return  [
            'width' => $w,
            'height' => $h,
            'left' => $l,
            'top' => $t,
        ];
    }
    protected function getCoordinates($arr){
        return 'transform: translate('.$arr['left'].'px, '.$arr['top'].'px); width: ' . $arr['width'] . 'px; height: ' . $arr['height'] . 'px;';

    }
    protected function setId($id){
        $this->id = $id;
    }
    protected function getId(){
        return $this->id;
    }
    protected function setData($id){

        $this->setId($id);
        $invoice  =  \DB::table('inv_invoices')->where('customer_id',1)->where('id',$id)->first();

         $this->invoice = (array) $invoice;
    }
    protected function getData(){

        return $this->invoice;
    }
    protected function rulers(){

        $data = $this->getData();

        $invoice_width = $data['width'];
        $invoice_height = $data['height'];

        $array['width'] = $invoice_width*38;
        $array['height'] = $invoice_height*38;
        $array['image'] = url('invImages/'.$data['image']);

        for($i=0;$i<=$invoice_height;$i++){

            $carpim = $i*38;
            $array['v'][] = '<div class="Designer-ruler-v-tick" style="height:38px;top:'.$carpim.'px;">
                    <div class="Designer-ruler-v-number">'.$i.'</div>
                    <div class="Designer-ruler-v-half-tick" style="height:16px"></div>
                </div>';

        }
        for($ix=0;$ix<=$invoice_width;$ix++){
            $carpim = $ix*38;

            $array['h'][] = '<div class="Designer-ruler-h-tick" style="width:38px;left:'.$carpim.'px;">
                    <div class="Designer-ruler-h-number">'.$ix.'</div>
                    <div class="Designer-ruler-h-half-tick" style="width:16px"></div>
                </div>';
        }


        return response()->json($array);

    }
    protected function getMenu(){

       $menus =   \DB::table('inv_items')->get()->toArray();

        foreach($menus as  $m){
            $m->text = $m->title;
            $menu[] = (array) $m;
        }

        // $menu[] = ['id'=>2,'title'=>'Müşteri Adresi','icon'=>'fa-envelope','width'=>200,'height'=>100,'attr'=>'text','text'=>'adres satiri'];


        return $menu;




    }
    protected function getList($item){
        $text = ' <div id="ember'.$item['id'].'"  data-id="'.$item['id'].'" data-attr="'.$item['attr'].'" class=" Designer-field resizable draggable ui-resizable"
                     style="position: absolute; '.$item['style'].'" data-x="'.$item['coordinates']['left'].'" data-y="'.$item['coordinates']['top'].'" ><p
                            style="font-family: PT Sans;font-size: 12pt;text-align: left;font-style: normal;font-weight: normal;">
                            '.$item['text'].'
                    </p>
                    <i class="fa fa-times" data-ember-action="'.$item['id'].'"></i>
                    <div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div>
                    <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                    <div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se"
                         style="z-index: 90;"></div>
                </div>';

        return $text;
    }
    protected function getTable($item,$columns,$rows){

       /// dd($columns);
        $cols['stock_code'] = 'ÜRÜN KODU';
        $cols['amount'] = 'MİKTAR';
        $cols['price'] = 'TUTAR';


        $tr = '';

        foreach($columns as $col){
            $th[] = $cols[$col];

        }
        foreach(array_chunk($rows,count($th)) as $row){
            $tr.='<tr>';
            foreach($row as $r){
                $tr.='<td>'.$r.'</td>';
            }
            $tr.='</tr>';
        }


        $text = '<table style="z-index: -1" class="table table-bordered" cellpadding="3" cellspacing="3">';
        $text.='<thead>';
        $text.='<tr>';
            foreach($th as $ts){
                $text.='<th>'.$ts.'</th>';
            }
        $text.='</tr>';
        $text.='</thead>';
        $text.=' <tbody>';

            $text.=$tr;


        $text.=' </tbody>';

        $text.='</table>';


        return '<div id="ember'.$item['id'].'"  data-id="'.$item['id'].'" data-attr="'.$item['attr'].'" class=" Designer-field resizable draggable ui-resizable"
                     style="position: absolute; '.$item['style'].'" data-x="'.$item['coordinates']['left'].'" data-y="'.$item['coordinates']['top'].'" >
                     <p class="table-responsive">'.$text.'</p> <i class="fa fa-times" data-ember-action="'.$item['id'].'"></i>
                    <div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div>
                    <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                    <div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se"
                         style="z-index: 90;"></div>
                </div>';
    }
    protected function getText($title,$attr,$item=false ){
        $file = $this->json;
        $data = json_decode($file);


        if(in_array($attr,$this->list)){
            return $title.': '.$data->attr->{$attr};
        }

        return  $data->attr->{$attr};
    }
    protected function listItems($id){


       $items = \DB::table('inv_invoice_items as it')->where('it.invoice_id',$id)
           ->join('inv_item_coordinates as ic','ic.item_id','=','it.id')
           ->join('inv_items as items','items.attr','=','it.attr')
       ->select('items.id','ic.item_id','items.attr','ic.width','ic.height','ic.left','ic.top','items.title','it.type');
       if(!$items->count()){
           $index[] = [];
       }
        foreach($items->get() as $item){

           $itemsCoordinates = [
               'width' => $item->width,
               'height' => $item->height,
               'left' => $item->left,
               'top' => $item->top,
           ];
           $index[] = [
               'id' => $item->id,
               'text' => $this->getText($item->title,$item->attr,$item),
               'attr' => $item->attr,
               'type' => $item->type,
               'style' => $this->getCoordinates($itemsCoordinates),
               'coordinates'=>$itemsCoordinates
           ];
       }



        return response()->json($index);
    }
    protected function listItemsWithArray($id){


       $items = \DB::table('inv_invoice_items as it')->where('it.invoice_id',$id)
           ->join('inv_item_coordinates as ic','ic.item_id','=','it.id')
           ->join('inv_items as items','items.attr','=','it.attr')
       ->select('items.id','ic.item_id','items.attr','it.sorting_list','ic.width','ic.height','ic.left','ic.top','items.title','it.type');
       if(!$items->count()){
           $index[] = [];
       }
        foreach($items->get() as $item){

           $itemsCoordinates = [
               'width' => $item->width,
               'height' => $item->height,
               'left' => $item->left,
               'top' => $item->top,
           ];
           $index[] = [
               'id' => $item->id,
               'text' => $this->getText($item->title,$item->attr,$item),
               'attr' => $item->attr,
               'type' => $item->type,
               'style' => $this->getCoordinates($itemsCoordinates),
               'coordinates'=>$itemsCoordinates
           ];
       }



        return $this->listsWithArray($index);
    }
    protected function getItems($request){
        return $this->listItems($this->getId());
    }
    protected function listsWithArray($data){
        $html = '';
        if(isset($data)){

            foreach($data as $item) {
                if(!isset($item['type'])){
                    return '';
                }
                if ($item['type'] == 'txt'):

                    $html .= ' <div id="ember' . $item['id'] . '"  data-id="' . $item['id'] . '" data-attr="' . $item['attr'] . '" class=" Designer-field resizable draggable ui-resizable"
                     style="position: absolute; ' . $item['style'] . '" data-x="' . $item['coordinates']['left'] . '" data-y="' . $item['coordinates']['top'] . '" ><p
                            style="font-family: PT Sans;font-size: 12pt;text-align: left;font-style: normal;font-weight: normal;">
        ' . $item['text'] . '
                    </p>';


                    $html .= '<i class="fa fa-times" data-ember-action="' . $item['id'] . '"></i>
                    <div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div>
                    <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                    <div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se"
                         style="z-index: 90;"></div>';

                    $html .= '</div>';
                endif;
            }
        }

        return $html;
    }
    protected function lists($request){
        $input = $request->all();
        $preview = false;
        if(isset($input['preview'])){
            $preview = true;
        }

        if(isset($input['data'])){
            $data = $input['data'];
        } else {
            $data = [];
        }



        $html = '';
        if(count($data) > 0){

            foreach($data as $item){

                if($item['type'] == 'txt'):

                    $html.= ' <div id="ember'.$item['id'].'"  data-id="'.$item['id'].'" data-attr="'.$item['attr'].'" class=" Designer-field resizable draggable ui-resizable"
                     style="position: absolute; '.$item['style'].'" data-x="'.$item['coordinates']['left'].'" data-y="'.$item['coordinates']['top'].'" ><p
                            style="font-family: PT Sans;font-size: 12pt;text-align: left;font-style: normal;font-weight: normal;">
        '.$item['text'].'
                    </p>';

                if(!$preview) {


                    $html .= '<i class="fa fa-times" data-ember-action="' . $item['id'] . '"></i>
                    <div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div>
                    <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                    <div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se"
                         style="z-index: 90;"></div>';
                }
                    $html.= '</div>';


                endif;
            }
        }


        /* $menus = $this->getMenu();
        $menu = '';
        foreach($menus as $m){
            $menu.='<li data-id="'.$m['id'].'" data-text="'.$m['text'].'" data-width="'.$m['width'].'"  data-height="'.$m['height'].'"  data-attr="'.$m['attr'].'" class="draggable menu_item  u-borderBottom Designer-draggableTool  ">
                            <i class="fa '.$m['icon'].' "></i>
                           '.$m['title'].'
                        </li>';
        }*/


        return ['items'=>$html];
    }
    protected function deleteInvoiceItems($id){
       $items =  \DB::table('inv_invoice_items')->where('invoice_id',$id)->get();
        foreach($items as $item){
            $item_id = $item->id;
            \DB::table('inv_item_coordinates')->where('item_id',$item_id)->delete();
        }
        \DB::table('inv_invoice_items')->where('invoice_id',$id)->delete();
    }
    protected function invoiceUpdate($data){
        $customer_id = $data['customer_id'];

        $name = 'İsimsiz Şablon';
        if(isset($data['invoce_name'])){
            $name = $data['invoce_name'];
        }

        $type = $data['size'];
        $layout = $data['layout'];


        $width =  $data['width'] ;
        $height =  $data['height'] ;
        $id = $data['invoice_id'];

        $nData = [
            'customer_id'=>$customer_id,
            'invoice_name'=>$name,
            'type'=>$type,
            'layout'=>$layout,

            'width'=>$width,
            'height'=>$height,
        ];

        if(isset($data['file'])){
            $image =  $data['file'] ;
            $nData = [
                'customer_id'=>$customer_id,
                'invoice_name'=>$name,
                'type'=>$type,
                'layout'=>$layout,
                'image'=>$image,
                'width'=>$width,
                'height'=>$height,
            ];
        }
        $invoice_id = \DB::table('inv_invoices')->where('id',$id)->update($nData);
    }
    protected function newInvoice($data){


        $customer_id = $data['customer_id'];

        $name = 'İsimsiz Şablon';
        if(isset($data['invoce_name'])){
            $name = $data['invoce_name'];
        }

        $type = $data['size'];
        $layout = $data['layout'];
        $image =  $data['file'] ;
        $width =  $data['width'] ;
        $height =  $data['height'] ;
       $invoice_id = \DB::table('inv_invoices')->insertGetId([
           'customer_id'=>$customer_id,
           'invoice_name'=>$name,
           'type'=>$type,
           'layout'=>$layout,
           'image'=>$image,
            'width'=>$width,
               'height'=>$height,
           ]
       );
        foreach($data['data'] as   $d){

            $data = [
                'invoice_id'=>$invoice_id,
                'attr'=>$d->attr,
                'type'=>'txt'
            ];
            $item_id = \DB::table('inv_invoice_items')->insertGetId($data);
            $data_2 = ['item_id'=>$item_id,'width'=>$d->width,'height'=>$d->height,'left'=>$d->left,'top'=>$d->top];
            \DB::table('inv_item_coordinates')->insert($data_2);
        }


        return $invoice_id;

    }
    protected function capture($request){
        $input = $request->all();
        $data =  json_decode($input['data']);

        $invoice_id = $input['invoice_id'];
        if(!$invoice_id){
            $input['file'] = $this->uploadImage($request);
            $input['data'] =  json_decode($input['data']);
            return $this->newInvoice($input);
        }


        $this->deleteInvoiceItems($invoice_id);
        $input['file'] = $this->uploadImage($request);
        $input['data'] =  json_decode($input['data']);
        $this->invoiceUpdate($input);
        foreach($data as   $d){

            if($d->attr == 'hizmet'){
                $data = [
                    'invoice_id'=>$invoice_id,
                    'attr'=>$d->attr,
                    'type'=>'txt',
                    'sorting_list'=>json_encode($d->table)
                ];
            } else {
                $data = [
                    'invoice_id'=>$invoice_id,
                    'attr'=>$d->attr,
                    'type'=>'txt',
                    'sorting_list'=>'',
                ];
            }


            $item_id = \DB::table('inv_invoice_items')->insertGetId($data);
            $data_2 = ['item_id'=>$item_id,'width'=>$d->width,'height'=>$d->height,'left'=>$d->left,'top'=>$d->top];
            \DB::table('inv_item_coordinates')->insert($data_2);

        }

    }
    protected  function uploadImage($request){

        $input['image'] = '';
        if ($request->hasFile('image')) {

            $ext = $request->file('image')->getClientOriginalExtension();
            $fileName = rand(1000,9999999).'.'.$ext;
            if(file_exists(public_path('invImages/'.$fileName))){
                $fileName = rand(84687,99999999).'.'.$ext;
            }

            $request->file('image')->move(public_path('invImages'), $fileName);
            $input['image'] =$fileName;
        }

        return $input['image'];
    }
    protected function store($request){

        $input = $request->all();

        if ($request->hasFile('image')) {

            $ext = $request->file('image')->getClientOriginalExtension();
            $fileName = rand(1000,9999999).'.'.$ext;
            if(file_exists(public_path('invImages/'.$fileName))){
                $fileName = rand(84687,99999999).'.'.$ext;
            }

            $request->file('image')->move(public_path('invImages'), $fileName);
            $input['image'] =$fileName;
        }

        unset($input['_token']);
        $input['customer_id'] = 1;
        if(!isset($input['invoice_name'])){
            $input['invoice_name'] = 'Test';
        }
        $inv = \DB::table('inv_invoices')->insertGetId($input);
        return redirect('/invoice/detail/'.$inv);


    }

}
