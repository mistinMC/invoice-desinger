<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvoiceWizard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('inv_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('icon');
            $table->string('width');
            $table->string('height');
            $table->string('attr');

        });
        Schema::create('inv_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_id')->index();
            $table->string('invoice_name');
            $table->enum('type',['A4','A5','A3','Custom'])->default('A4');
            $table->enum('layout',['yatay','dikey'])->default('dikey');
            $table->string('width');
            $table->string('height');
            $table->string('image');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('inv_invoice_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_id')->index();
            $table->string('attr');
            $table->string('type');
            $table->timestamp('created_at')->nullable();
        });
        Schema::create('inv_invoice_item_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_id')->index();
            $table->string('description');

        });

        Schema::create('inv_item_coordinates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_id')->index();
            $table->string('width');
            $table->string('height');
            $table->string('left');
            $table->string('top');
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //'
        Schema::drop('inv_items');
        Schema::drop('inv_invoices');
        Schema::drop('inv_invoice_items');
        Schema::drop('inv_item_coordinates');
        Schema::drop('inv_invoice_item_descriptions');
    }
}
