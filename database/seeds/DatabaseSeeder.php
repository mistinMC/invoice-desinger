<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu[] = ['id'=>1,'title'=>'Müşteri Ünvanı','icon'=>'fa-building-o','width'=>200,'height'=>100,'attr'=>'unvan','text'=>'Teknosa LTD'];
        $menu[] = ['id'=>2,'title'=>'Müşteri Adresi','icon'=>'fa-envelope','width'=>200,'height'=>100,'attr'=>'adres','text'=>'adres satiri'];
        $menu[] = ['id'=>3,'title'=>'Müşteri Telefon Numarası','icon'=>'fa-phone','width'=>200,'height'=>100,'attr'=>'musteri-tel','text'=>'0532 123  4567'];
        $menu[] = ['id'=>4,'title'=>'Vergi Bilgileri ( Daire ve Numara )','icon'=>'fa-legal','width'=>100,'height'=>100,'attr'=>'vergi-bilgileri','text'=>'buyukcekmece / 13565762413'];
        $menu[] = ['id'=>5,'title'=>'Vergi Dairesi','icon'=>'fa-legal','width'=>200,'height'=>100,'attr'=>'vergi-dairesi','text'=>'buyukcekmece'];
        $menu[] = ['id'=>6,'title'=>'Vergi Numarası','icon'=>'fa-legal','width'=>200,'height'=>100,'attr'=>'vergi-numarasi','text'=>'23415465'];
        $menu[] = ['id'=>7,'title'=>'Düzenlenme Tarihi','icon'=>'fa-calendar','width'=>100,'height'=>200,'attr'=>'duzenlenme-tarihi','text'=>'15/11/2016'];
        $menu[] = ['id'=>8,'title'=>'Fatura No','icon'=>'fa-file-text-o','width'=>200,'height'=>100,'attr'=>'fatura-no','text'=>'132545'];
        $menu[] = ['id'=>9,'title'=>'İrsaliye No','icon'=>'fa-truck','width'=>200,'height'=>100,'attr'=>'irsaliye','text'=>'5433'];
        $menu[] = ['id'=>10,'title'=>'Fiili Sevk Tarihi','icon'=>'fa-truck','width'=>200,'height'=>100,'attr'=>'sevk-tarihi','text'=>'15/11/2016'];
        $menu[] = ['id'=>11,'title'=>'Fiili Sevk Saati','icon'=>'fa-truck','width'=>200,'height'=>100,'attr'=>'sevk-saati','text'=>'15:10'];
        $menu[] = ['id'=>12,'title'=>'Sevk Adresi','icon'=>'fa-truck','width'=>200,'height'=>100,'attr'=>'sevk-adresi','text'=>'sevk adres satiri'];
        $menu[] = ['id'=>13,'title'=>'Ödeme Tarihi','icon'=>'fa-calendar','width'=>100,'height'=>200,'attr'=>'odeme-tarihi','text'=>'15/11/2016'];
        $menu[] = ['id'=>14,'title'=>'Müşteri Bakiyesi','icon'=>'fa-building','width'=>100,'height'=>200,'attr'=>'musteri-bakiyesi','text'=>'100'];
        $menu[] = ['id'=>15,'title'=>'Hizmet / Ürün Kalemleri','icon'=>'fa-list','width'=>800,'height'=>600,'attr'=>'hizmet','text'=>'buraya tablo gelcek'];
        $menu[] = ['id'=>16,'title'=>'Ara Toplam','icon'=>'fa-plus','width'=>100,'height'=>200,'attr'=>'ara-toplam','text'=>'21,000'];
        $menu[] = ['id'=>17,'title'=>'Toplam İndirim','icon'=>'fa-minus','width'=>100,'height'=>200,'attr'=>'indirim','text'=>'0'];
        $menu[] = ['id'=>18,'title'=>'Toplam Kdv','icon'=>'fa-institution','width'=>100,'height'=>200,'attr'=>'kdv','text'=>'2,500'];
        $menu[] = ['id'=>19,'title'=>'Vergi Matrahı','icon'=>'fa-minus-square','width'=>100,'height'=>200,'attr'=>'vergi-matrahi','text'=>'100'];
        $menu[] = ['id'=>20,'title'=>'Genel Toplam','icon'=>'fa-plus-square','width'=>100,'height'=>200,'attr'=>'genel-toplam','text'=>'23,600'];
        $menu[] = ['id'=>21,'title'=>'Yazı İle Toplam','icon'=>'fa-font','width'=>100,'height'=>200,'attr'=>'yazi-genel-toplam','text'=>'yirmiüçbinaltıyüztürklirası'];

        foreach($menu as $m){

            unset($m['text']);
            DB::table('inv_items')->insert($m);

        }

        $invoices = ['customer_id'=>1,'invoice_name'=>'deneme faturasi','type'=>'A4','layout'=>'dikey','width'=>'21','height'=>'30','image'=>'test.jpg'];
        $invoice = DB::table('inv_invoices')->insertGetId($invoices);
        $item = DB::table('inv_invoice_items')->insertGetId(['invoice_id'=>$invoice,'attr'=>'unvan','type'=>'txt']);
        DB::table('inv_item_coordinates')->insert(['item_id'=>$item,'width'=>'100','height'=>'50','left'=>'200','top'=>'20']);
    }
}
