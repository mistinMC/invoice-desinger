@extends('inv.layouts.inv')
@section('options')
    <div class="saveArea hide " style="margin-top: 5px;">
        <div class="col-md-4"> Fatura Şablonu değişiklikler algılandı.</div>
        <div class="col-md-4"> <input id="inv_name" class="form-control"  placeholder="Şablon Adı"> </div>
        <button   class="btn btn-primary center-block capture  ">KAYDET</button>
    </div>
@endsection
@push('style')
<link rel="stylesheet" href="/jquery-ui.css">
<style>
    @import url("http://fonts.googleapis.com/css?family=Roboto:400,300");
    body {
        font-family: "Roboto", sans-serif;
        background: rgb(204,204,204);
    }
    section {
        background: white;
        display: block;
        margin: 0 auto;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);

        background-repeat: no-repeat;
    }
    section[size="A4"] {
        width: 21cm;
        height: 29.7cm;
    }
    section[size="A4"][layout="landscape"] {
        width: 29.7cm;
        height: 21cm;
    }
    section[size="A3"] {
        width: 29.7cm;
        height: 42cm;
    }
    section[size="A3"][layout="landscape"] {
        width: 42cm;
        height: 29.7cm;
    }
    section[size="A5"] {
        width: 14.8cm;
        height: 21cm;
    }
    section[size="A5"][layout="landscape"] {
        width: 21cm;
        height: 14.8cm;
    }
    @media print {
        body, section {
            margin: 0;
            box-shadow: 0;
        }
    }
    .context-menu {
        display: none;
        position: absolute;
        z-index: 10;
        padding: 12px 0;
        width: 270px;
        background-color: #fff;
        border: solid 1px #dfdfdf;
        box-shadow: 1px 1px 2px #cfcfcf;
    }

    .context-menu--active {
        display: block;
    }

    .context-menu__items {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .context-menu__item {
        display: block;
        margin-bottom: 4px;
    }

    .context-menu__item:last-child {
        margin-bottom: 0;
    }

    .context-menu__link,.image {
        display: block;
        padding: 4px 12px;
        color: #0066aa;
        text-decoration: none;
    }

    .context-menu__link:hover {
        color: #fff;
        background-color: #0066aa;
    }
    .image:hover {
        color: #fff;
        background-color: #0066aa;
    }
    .Designer-field{
        color:#000;
        border: 1px solid #dadada !important;
        position: absolute;
    }
    .Designer-field .fa {
        cursor:pointer;
        position: absolute;
        top: 0;
        right: 0px;
        padding: 4px 5px 5px 6px;
        background-color: #e8e7e7;
        -webkit-transition-property: color;
        -webkit-transition-duration: .66s;
        -webkit-transition-timing-function: cubic-bezier(0.25,1,.25,1);
        -moz-transition-property: color;
        -moz-transition-duration: .66s;
        -moz-transition-timing-function: cubic-bezier(0.25,1,.25,1);
        -ms-transition-property: color;
        -ms-transition-duration: .66s;
        -ms-transition-timing-function: cubic-bezier(0.25,1,.25,1);
        -o-transition-property: color;
        -o-transition-duration: .66s;
        -o-transition-timing-function: cubic-bezier(0.25,1,.25,1);
        transition-property: color;
        transition-duration: .66s;
        transition-timing-function: cubic-bezier(0.25,1,.25,1);
    }
    .disabled {
        pointer-events:none;
        opacity:0.6;
        color:#dadada
    }
    .saveArea{

    }
    .A4_BackSize_portrait{
        background-size: 21cm 29.7cm!important;
    }

    .A5_BackSize_portrait{
        background-size: 14.8cm 21cm!important;
    }
    .A5_BackSize_landscape{
        background-size: 21cm 14.8cm!important;
    }


    .A4_BackSize_landscape{
        background-size: 29.7cm 21cm!important;
    }
</style>
@endpush
@section('content')
    <BR><BR>
    <BR><BR>

    <img id="img" style="display: none">
    <section size="A4" layout="portrait" style="width:21cm; height:29.7cm;background-size:21cm 29.7cm , cover " class="task"></section>


    <nav id="context-menu" class="context-menu">
        <ul class="context-menu__items">
            <li data-id="1" data-text="" data-width="" data-height="" data-attr="" class="">
                <a href="#" class="image"><i class="fa fa-picture-o"></i> Fatura İmajı</a>
            </li>
            @foreach($menu_items as $m)
                <li  data-id="{{$m['id']}}" data-text="{{$m['text']}}" data-width="{{$m['width']}}"  data-height="{{$m['height']}}"  data-attr="{{$m['attr']}}" class="context-menu__item">
                    <a href="#" class="context-menu__link" data-action="{{$m['title']}}"><i class="fa {{$m['icon']}}"></i> {{$m['title']}}</a>
                </li>
            @endforeach

        </ul>
    </nav>
@endsection
@push('scripts')
<script src="/assets/js/ajaxTools.js"></script>
<script src="/jquery-ui.js"></script>
<script>


    (function() {

        "use strict";

        /**
         * Variables.
         */
        var contextMenuClassName = "context-menu";
        var contextMenuItemClassName = "context-menu__item";
        var contextMenuLinkClassName = "context-menu__link";
        var contextMenuActive = "context-menu--active";

        var taskItemClassName = "task";
        var taskItemInContext;

        var clickCoords;
        var clickCoordsX;
        var clickCoordsY;

        var menu = document.querySelector("#context-menu");
        var menuItems = menu.querySelectorAll(".context-menu__item");
        var menuState = 0;
        var menuWidth;
        var menuHeight;
        var menuPosition;
        var menuPositionX;
        var menuPositionY;

        var windowWidth;
        var windowHeight;
        var loadImg;
        var url = '/invapi';
        var defaults = {
            cm:38, //1 cm 38pixele esit;
            pixel : 0.02645833, // 1px cm e esit
        }
        var section = {
            layout :     $("section").attr('layout'),
            height : Math.round($('section').height() * defaults.pixel),
            width :  Math.round($('section').width() * defaults.pixel)
        }
        console.log('sectioon ')
        console.log(section);
        var products = {};
        function pageSetupOpt(){
            $(".save").on('click',function(){
                var height = $('input[name=hght]').val();
                var  width = $('input[name=hght]').val();
                section.width = width;
                section.height = height;

                $("section").attr('size','Custom');
                $("section").attr('layout','Custom');
                $(".pageScape").addClass('disabled');
                console.log
                $("section").css({width:width+'cm',height:height+'cm'});
                $("section").css("background-size", width+'cm '+height+'cm, cover');

                $("#myModal2").modal('hide');
            })
        }
        $('.boyut').on('click',function(){
            $(".boyut").removeClass('active');
            var id = $(this).data('id');
            $(this).addClass('active');
            if(id == 'custom'){
                $("#myModal2").find('.modal-title').html('Özel Sayfa Boyutu İçin Değer Giriniz');
                $("#myModal2").find('.modal-body').css('min-height','150px');
                $("#myModal2").find('.modal-body').html('<div class="col-md-6"><div class="form-group">' +
                        '<span><i class="fa fa-long-arrow-right"></i> Sayfa Eni</span>' +
                        '<input type="number" name="wdth" placeholder="CM" class="form-control" > ' +
                        '</div></div>'+'<div class="col-md-6"><div class="form-group">' +
                        '<span><i class="fa fa-long-arrow-down"></i> Sayfa Boyu</span>' +
                        '<input  type="number"  name="hght"   placeholder="CM" class="form-control " > ' +
                        '</div></div>'

                );
                $("#myModal2").find('.modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>'+
                        '<button type="button" class="btn save btn-primary">Ayarları Kaydet</button>')
                $("#myModal2").modal('show');
                pageSetupOpt()
                return ;
            }


            if(id != 'custom'){
                $(".pageScape").removeClass('disabled');
                $('section').css({width:'',height:''})
            }
            var layout = $('.layout .active').data('layout')

            $('section').attr('size',id);
            $('section').attr('layout',layout);

        });

        $('.layout').on('click',function(){
            $(".layout").removeClass('active')
            var id = $(this).data('layout');
            $(this).addClass('active');

            section.size = $('section').attr('size');
            section.layout = id;
            $('section').attr('layout',id);
        });

        $(".context-menu__item").on('click',function(){
            var id = ($(this).data('id'));
            $(this).addClass('disabled');
            $('.saveArea').removeClass('hide');
            getOptions($(this),clickCoordsX,clickCoordsY);
        });

        $(".image").on('click',function(){

            $("#myModal").modal('show');

            $("#imgInp").change(function(){
                readURL(this);
            });

        })
        $(".capture").on('click',function(){
            capture();
        })


        function capture() {

            var file_data = $('#imgInp').prop('files')[0];
            var form_data = new FormData();
            form_data.append('image', file_data);

            var datas = [];
            $.each($(".Designer-field"),function(index,value){
                var attr = $(this).data('attr');
                var height = $(this).height();
                var width = $(this).width();
                var left = $(this).data('x');
                var top = $(this).data('y');
                datas.push({attr:attr,height:height,width:width,left:left,top:top});

            });

            var json_arr = JSON.stringify(datas);
            var name = $("#inv_name").val();
            var size = $('section').attr('size');
            var layout = $('section').attr('layout');
            form_data.append('data', json_arr);
            form_data.append('invoice_name', name);
            form_data.append('size', size);
            form_data.append('layout', layout);
            form_data.append('invoice_id', 0);
            form_data.append('customer_id', 1);
            form_data.append('width', section.width);
            form_data.append('height',section.height  );


            $.ajax({
                url: url+'/capture',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    window.location.href = '{{url('/invoice/preview')}}/'+response;
                }
            });
            /*




             postData(url+'/capture',{data:json_arr,invoice_id:0,customer_id:1,invoice_name:name,size:size,layout:layout},'POST','text',function(data){

             })*/

        }
        $("#form1").on('submit',function(){

        })

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $("section").css('background-image','url('+ e.target.result+')');
                    $("#img").attr('src', e.target.result);
                    var img = document.getElementById('img');
//or however you get a handle to the IMG
                    var width = img.width;
                    var height = img.height;

                    if(width > 1050){
                        var size = $('section').attr('size');
                        var layout = $('section').attr('layout');
                        $("section").addClass(size+'_BackSize_'+layout);

                    }
                    $("#myModal").modal('hide');

                }
                loadImg = input.files[0];
                reader.readAsDataURL(input.files[0]);
            }
        }


        function loadTemplate(url,datas,callback){

            postData(url,datas,'POST','text',function(data){

                callback(data)
            })

        }
        function getOptions(selector,x,y){
            var txt = selector.data('text');
            var atr = selector.data('attr');
            var width = selector.data('width');
            var height = selector.data('height');
            var id = selector.data('id');
            var left = x - 270;
            var top = y-100;





            loadTemplate(url+'/addItem',{id:id,text:txt,attr:atr,w:width,h:height,l:left,t:top},function(data) {

                $('section').append(data);


                interact('.Designer-field')
                        .draggable({
                            onmove: window.dragMoveListener,
                            onend:window.endListener,
                        })
                        .resizable({
                            preserveAspectRatio: false,
                            edges: {left: false, right: true, bottom: true, top: false},
                            onend:window.endListener,
                        })
                        .on('resizemove', function (event) {
                            var target = event.target,
                                    x = (parseFloat(target.getAttribute('data-x')) || 0),
                                    y = (parseFloat(target.getAttribute('data-y')) || 0);

                            // update the element's style
                            target.style.width = event.rect.width + 'px';
                            target.style.height = event.rect.height + 'px';

                            // translate when resizing from top or left edges
                            x += event.deltaRect.left;


                            target.style.webkitTransform = target.style.transform =
                                    'translate(' + x + 'px,' + y + 'px)';

                            target.setAttribute('data-x', x);
                            target.setAttribute('data-y', y);
                            //target.textContent = Math.round(event.rect.width) + '×' + Math.round(event.rect.height);
                        });

                $('.Designer-field').on('click', '.fa-times', function () {

                    var id = $(this).attr('data-ember-action');

                    $("#ember" + id).remove();
                    $("html").css('cursor','default');
                    $("#features").addClass('hide')
                    $("#features").addClass('closed')
                    $('.context-menu__item[data-id='+id+']').removeClass('disabled');
                })


            });


        }


        function endListener(event){
            var target = event.target;
            var object = {
                id:target.getAttribute('data-id'),
                x:target.getAttribute('data-x'),
                y:target.getAttribute('data-y'),
                w:target.offsetWidth,
                h:target.offsetHeight,
            }


        }
        function dragMoveListener(event) {



            var target = event.target,

            // keep the dragged position in the data-x/data-y attributes
                    x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                    y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

            // translate the element
            target.style.webkitTransform =
                    target.style.transform =
                            'translate(' + x + 'px, ' + y + 'px)';

            // update the posiion attributes
            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);

        }

        // this is used later in the resizing and gesture demos
        window.dragMoveListener = dragMoveListener;




        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //
        // H E L P E R    F U N C T I O N S
        //
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////

        /**
         * Function to check if we clicked inside an element with a particular class
         * name.
         *
         * @param {Object} e The event
         * @param {String} className The class name to check against
         * @return {Boolean}
         */
        function clickInsideElement( e, className ) {
            var el = e.srcElement || e.target;

            if ( el.classList.contains(className) ) {
                return el;
            } else {
                while ( el = el.parentNode ) {
                    if ( el.classList && el.classList.contains(className) ) {
                        return el;
                    }
                }
            }

            return false;
        }

        /**
         * Get's exact position of event.
         *
         * @param {Object} e The event passed in
         * @return {Object} Returns the x and y position
         */
        function getPosition(e) {
            var posx = 0;
            var posy = 0;

            if (!e) var e = window.event;

            if (e.pageX || e.pageY) {
                posx = e.pageX;
                posy = e.pageY;
            } else if (e.clientX || e.clientY) {
                posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
            }

            return {
                x: posx,
                y: posy
            }
        }

        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //
        // C O R E    F U N C T I O N S
        //
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////



        /**
         * Initialise our application's code.
         */
        function init() {
            contextListener();
            clickListener();
            keyupListener();
            resizeListener();
        }

        /**
         * Listens for contextmenu events.
         */
        function contextListener() {
            document.addEventListener( "contextmenu", function(e) {
                taskItemInContext = clickInsideElement( e, taskItemClassName );

                if ( taskItemInContext ) {
                    e.preventDefault();
                    toggleMenuOn();
                    positionMenu(e);
                } else {
                    taskItemInContext = null;
                    toggleMenuOff();
                }
            });
        }

        /**
         * Listens for click events.
         */
        function clickListener() {
            document.addEventListener( "click", function(e) {
                var clickeElIsLink = clickInsideElement( e, contextMenuLinkClassName );

                if ( clickeElIsLink ) {
                    e.preventDefault();
                    menuItemListener( clickeElIsLink );
                } else {
                    var button = e.which || e.button;
                    if ( button === 1 ) {
                        toggleMenuOff();
                    }
                }
            });
        }

        /**
         * Listens for keyup events.
         */
        function keyupListener() {
            window.onkeyup = function(e) {
                if ( e.keyCode === 27 ) {
                    toggleMenuOff();
                }
            }
        }

        /**
         * Window resize event listener
         */
        function resizeListener() {
            window.onresize = function(e) {
                toggleMenuOff();
            };
        }

        /**
         * Turns the custom context menu on.
         */
        function toggleMenuOn() {
            if ( menuState !== 1 ) {
                menuState = 1;
                menu.classList.add( contextMenuActive );
            }
        }

        /**
         * Turns the custom context menu off.
         */
        function toggleMenuOff() {
            if ( menuState !== 0 ) {
                menuState = 0;
                menu.classList.remove( contextMenuActive );
            }
        }

        /**
         * Positions the menu properly.
         *
         * @param {Object} e The event
         */
        function positionMenu(e) {
            clickCoords = getPosition(e);
            clickCoordsX = clickCoords.x;
            clickCoordsY = clickCoords.y;

            menuWidth = menu.offsetWidth + 4;
            menuHeight = menu.offsetHeight + 4;

            windowWidth = window.innerWidth;
            windowHeight = window.height;





            if ( (windowWidth - clickCoordsX) < menuWidth ) {
                menu.style.left = windowWidth - menuWidth + "px";
            } else {
                menu.style.left = clickCoordsX + "px";
            }

            if ( (windowHeight - clickCoordsY) < menuHeight ) {
                menu.style.top = windowHeight - menuHeight + "px";
            } else {
                menu.style.top = clickCoordsY + "px";
            }
        }

        /**
         * Dummy action function that logs an action when a menu item link is clicked
         *
         * @param {HTMLElement} link The link that was clicked
         */
        function menuItemListener( link ) {
            //console.log( "Task ID - " + taskItemInContext.getAttribute("data-id") + ", Task action - " + link.getAttribute("data-action"));
            toggleMenuOff();

        }

        /**
         * Run the app.
         */
        init();

    })();
</script>
@endpush