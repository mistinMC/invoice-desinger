@extends('inv.layouts.inv')
@section('options')
    <div class="saveArea  " style="margin-top: 5px;">

        <div class="col-md-6 center-block">
            <a  href="{{url('invoice/detail/'.$id)}}"  class="btn btn-success   ">DÜZENLE</a>
        </div>
    </div>
@endsection
@push('style')
<link rel="stylesheet" href="/jquery-ui.css">
<meta name="invoice" content="{{ $id }}">
<style>
    @import url("http://fonts.googleapis.com/css?family=Roboto:400,300");
    body {
        font-family: "Roboto", sans-serif;
        background: rgb(204,204,204);
    }
    section {
        background: white;
        display: block;
        margin: 0 auto;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);

        background-repeat: no-repeat;
    }
    section[size="A4"] {
        width: 21cm;
        height: 29.7cm;
    }
    section[size="A4"][layout="landscape"] {
        width: 29.7cm;
        height: 21cm;
    }
    section[size="A3"] {
        width: 29.7cm;
        height: 42cm;
    }
    section[size="A3"][layout="landscape"] {
        width: 42cm;
        height: 29.7cm;
    }
    section[size="A5"] {
        width: 14.8cm;
        height: 21cm;
    }
    section[size="A5"][layout="landscape"] {
        width: 21cm;
        height: 14.8cm;
    }
    @media print {
        body, section {
            margin: 0;
            box-shadow: 0;
        }
    }

    .A4_BackSize_portrait{
        background-size: 21cm 29.7cm!important;
    }

    .A5_BackSize_portrait{
        background-size: 14.8cm 21cm!important;
    }
    .A5_BackSize_landscape{
        background-size: 21cm 14.8cm!important;
    }


    .A4_BackSize_landscape{
        background-size: 29.7cm 21cm!important;
    }

    .context-menu {
        display: none;
        position: absolute;
        z-index: 10;
        padding: 12px 0;
        width: 270px;
        background-color: #fff;
        border: solid 1px #dfdfdf;
        box-shadow: 1px 1px 2px #cfcfcf;
    }

    .context-menu--active {
        display: block;
    }

    .context-menu__items {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .context-menu__item {
        display: block;
        margin-bottom: 4px;
    }

    .context-menu__item:last-child {
        margin-bottom: 0;
    }

    .context-menu__link,.image {
        display: block;
        padding: 4px 12px;
        color: #0066aa;
        text-decoration: none;
    }

    .context-menu__link:hover {
        color: #fff;
        background-color: #0066aa;
    }
    .image:hover {
        color: #fff;
        background-color: #0066aa;
    }
    .Designer-field{
        color:#000;
        border:none !important;
        position: absolute;
    }
    .Designer-field .fa {
        cursor:pointer;
        position: absolute;
        top: 0;
        right: 0px;
        padding: 4px 5px 5px 6px;
        background-color: #e8e7e7;
        -webkit-transition-property: color;
        -webkit-transition-duration: .66s;
        -webkit-transition-timing-function: cubic-bezier(0.25,1,.25,1);
        -moz-transition-property: color;
        -moz-transition-duration: .66s;
        -moz-transition-timing-function: cubic-bezier(0.25,1,.25,1);
        -ms-transition-property: color;
        -ms-transition-duration: .66s;
        -ms-transition-timing-function: cubic-bezier(0.25,1,.25,1);
        -o-transition-property: color;
        -o-transition-duration: .66s;
        -o-transition-timing-function: cubic-bezier(0.25,1,.25,1);
        transition-property: color;
        transition-duration: .66s;
        transition-timing-function: cubic-bezier(0.25,1,.25,1);
    }
    .disabled {
        pointer-events:none;
        opacity:0.6;
        color:#dadada
    }
    .saveArea{

    }
</style>
@endpush
@section('content')
<BR><BR>
<BR><BR>


<section size="{{$data->type}}" layout="{{$data->layout}}" @if($data->type == 'Custom') style="width:{{$data->width}}cm;height:{{$data->height}}cm;  background-size: {{$data->width}}cm {{$data->height}}cm!important;" @endif class="task"></section>


<nav id="context-menu" class="context-menu">
    <ul class="context-menu__items">
        <li data-id="1" data-text="" data-width="" data-height="" data-attr="" class="">
            <a href="#" class="image"><i class="fa fa-picture-o"></i> Fatura İmajı</a>
        </li>
        @foreach($menu_items as $m)
            <li  data-id="{{$m['id']}}" data-text="{{$m['text']}}" data-width="{{$m['width']}}"  data-height="{{$m['height']}}"  data-attr="{{$m['attr']}}" class="context-menu__item">
                <a href="#" class="context-menu__link" data-action="{{$m['title']}}"><i class="fa {{$m['icon']}}"></i> {{$m['title']}}</a>
            </li>
        @endforeach

    </ul>
</nav>
@endsection
@push('scripts')
<script src="/assets/js/ajaxTools.js"></script>
<script src="/jquery-ui.js"></script>
<script>


    (function() {
                 @if($data->type == 'Custom')
                    $(".pageScheme").addClass('disabled')
                    $(".pageScape").addClass('disabled')
                @endif

        var url = '/invapi';
        var defaults = {
            cm:38, //1 cm 38pixele esit;
        }
        var products = {};

        $('.boyut').on('click',function(){
            $(".boyut").removeClass('active')
            var id = $(this).data('id');
            $(this).addClass('active');
           var layout = $('.layout .active').data('layout')
            $('section').attr('size',id);
            $('section').attr('layout',layout);

        });

        $('.layout').on('click',function(){
            $(".layout").removeClass('active')
            var id = $(this).data('layout');

            $(this).addClass('active');
            var size = $('.boyut .active').data('id');


            $('section').attr('size',size);
            $('section').attr('layout',id);

        });






        function loadTemplate(url,datas,callback){

            postData(url,datas,'POST','text',function(data){

                callback(data)
            })

        }


        postData(url+"/getItems",{},'POST','text',function(data){


            loadTemplate(url+"/lists",{data:data,preview:1},function(dt){

                if(dt.items){
                    $('section').html(dt.items);
                }

                //$('#item_menu').html(dt.menu);

            })

        });



    })();
</script>
@endpush