@extends('inv.layouts.inv')
@section('options')
    <div class="saveArea  " style="margin-top: 5px;">

        <div class="col-md-6 center-block">
            <button   class="btn btn-primary  capture  ">KAYDET</button>
            <a  href="{{url('invoice/preview/'.$id)}}"  class="btn btn-info pre ">ÖNİZLE</a>
        </div>
        <div class="col-md-4 text hide"><i class="fa fa-floppy-o"></i> Değişiklikleri Kaydedin</div>
    </div>
@endsection
@push('style')
<link rel="stylesheet" href="/jquery-ui.css">
<link rel="stylesheet" href="/css/messenger.css">
<link rel="stylesheet" href="/css/messenger-theme-future.css">

<style>
    @import url("http://fonts.googleapis.com/css?family=Roboto:400,300");
    body {
        font-family: "Roboto", sans-serif;
        background: rgb(204,204,204);
    }
    section {
        background: white;
        display: block;
        margin: 0 auto;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
        @if(isset($data->image) && $data->image != '')
        background-image: url("{{url('invImages/'.$data->image)}}");
        @endif
        background-repeat: no-repeat;
    }
    section[size="A4"] {
        width: 21cm;
        height: 29.7cm;
    }
    section[size="A4"][layout="landscape"] {
        width: 29.7cm;
        height: 21cm;
    }
    section[size="A3"] {
        width: 29.7cm;
        height: 42cm;
    }
    section[size="A3"][layout="landscape"] {
        width: 42cm;
        height: 29.7cm;
    }
    section[size="A5"] {
        width: 14.8cm;
        height: 21cm;
    }
    section[size="A5"][layout="landscape"] {
        width: 21cm;
        height: 14.8cm;
    }
    @media print {
        body, section {
            margin: 0;
            box-shadow: 0;
        }
    }
    .context-menu {
        display: none;
        position: absolute;
        z-index: 10;
        padding: 12px 0;
        width: 270px;
        background-color: #fff;
        border: solid 1px #dfdfdf;
        box-shadow: 1px 1px 2px #cfcfcf;
    }

    .context-menu--active {
        display: block;
    }

    .context-menu__items {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .context-menu__item {
        display: block;
        margin-bottom: 4px;
    }

    .context-menu__item:last-child {
        margin-bottom: 0;
    }

    .context-menu__link,.image {
        display: block;
        padding: 4px 12px;
        color: #0066aa;
        text-decoration: none;
    }

    .context-menu__link:hover {
        color: #fff;
        background-color: #0066aa;
    }
    .image:hover {
        color: #fff;
        background-color: #0066aa;
    }
    .Designer-field{
        color:#000;
        border: 1px solid #dadada !important;
        position: absolute;
    }
    .Designer-field .fa {
        cursor:pointer;
        position: absolute;
        top: 0;
        right: 0px;
        padding: 4px 5px 5px 6px;
        background-color: #e8e7e7;
        -webkit-transition-property: color;
        -webkit-transition-duration: .66s;
        -webkit-transition-timing-function: cubic-bezier(0.25,1,.25,1);
        -moz-transition-property: color;
        -moz-transition-duration: .66s;
        -moz-transition-timing-function: cubic-bezier(0.25,1,.25,1);
        -ms-transition-property: color;
        -ms-transition-duration: .66s;
        -ms-transition-timing-function: cubic-bezier(0.25,1,.25,1);
        -o-transition-property: color;
        -o-transition-duration: .66s;
        -o-transition-timing-function: cubic-bezier(0.25,1,.25,1);
        transition-property: color;
        transition-duration: .66s;
        transition-timing-function: cubic-bezier(0.25,1,.25,1);
    }
    .disabled {
        pointer-events:none;
        opacity:0.6;
        color:#dadada
    }
    .saveArea{

    }
</style>
<style>
    #sortable1{
        border: 1px solid #eee;
        width: 142px;
        min-height: 20px;
        list-style-type: none;
        margin: 0;
        padding: 5px 0 0 0;
        float: left;
        margin-right: 10px;
    }
    #sortable2{
        width: 100%;
        border: 1px solid #000;
        min-height: 50px;
        float: left;
        padding: 5px 0 0 0;
    }
    #sortable1 li, #sortable2 li {
        margin: 0 5px 5px 5px;
        padding: 5px;
        font-size: 1.2em;
        width: 120px;

    }
    #sortable2 li {
        float:left
    }
</style>
@endpush
@section('content')
<BR><BR>
<BR><BR>


<section size="{{$data->type}}" layout="{{$data->layout}}"  style="width:{{$data->width}}cm;height:{{$data->height}}cm;background-size: {{$data->width}}cm {{$data->height}}cm!important;  "   class="task">
{!! $items !!}
</section>


<nav id="context-menu" class="context-menu">
    <ul class="context-menu__items">
        <li data-id="1" data-text="" data-width="" data-height="" data-attr="" class="">
            <a href="#" class="image"><i class="fa fa-picture-o"></i> Fatura İmajı</a>
        </li>
        @foreach($menu_items as $m)
            <li  data-id="{{$m['id']}}" data-text="{{$m['text']}}" data-width="{{$m['width']}}"  data-height="{{$m['height']}}"  data-attr="{{$m['attr']}}" class="context-menu__item">
                <a href="#" class="context-menu__link" data-action="{{$m['title']}}"><i class="fa {{$m['icon']}}"></i> {{$m['title']}}</a>
            </li>
        @endforeach

    </ul>
</nav>

<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Faturanızda yer alacak hizmet kalemlerini seçiniz</h4>
            </div>
            <div class="modal-body" style="min-height: 250px">
                <div class="row">
                    <div class="col-md-2">
                        <ul id="sortable1" class="connectedSortable">
                            <li class="ui-state-default" data-value="stock_code">Ürün Kodu</li>
                            <li class="ui-state-default" data-value="amount">Miktar</li>
                            <li class="ui-state-default" data-value="price">Fiyat</li>

                        </ul></div>
                    <div class="col-md-10">
                        <ul id="sortable2" class="connectedSortable">

                        </ul>
                    </div>
                </div>







            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                <button type="button" class="btn btn-primary saveChanges">Değişiklikleri Kaydet</button>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="/assets/js/ajaxTools.js"></script>
<script src="/jquery-ui.js"></script>
<script src="/js/messenger.min.js"></script>
<script src="/js/messenger-theme-flat.js"></script>
<script>
    $( function() {
        $( "#sortable1, #sortable2" ).sortable({
            connectWith: ".connectedSortable"
        }).disableSelection();
    } );
</script>
<script>


    (function() {

        "use strict";

        /**
         * Variables.
         */
        var contextMenuClassName = "context-menu";
        var contextMenuItemClassName = "context-menu__item";
        var contextMenuLinkClassName = "context-menu__link";
        var contextMenuActive = "context-menu--active";

        var taskItemClassName = "task";
        var taskItemInContext;

        var clickCoords;
        var clickCoordsX;
        var clickCoordsY;

        var menu = document.querySelector("#context-menu");
        var menuItems = menu.querySelectorAll(".context-menu__item");
        var menuState = 0;
        var menuWidth;
        var menuHeight;
        var menuPosition;
        var menuPositionX;
        var menuPositionY;

        var windowWidth;
        var windowHeight;

        @if($data->type == 'Custom')
                   $(".pageScape").addClass('disabled')
                @endif

        var url = '/invapi';
        var defaults = {
            cm:38, //1 cm 38pixele esit;
            pixel : 0.02645833, // 1px cm e esit
        }
        var section = {
            layout :     $("section").attr('layout'),
            height : Math.round($('section').height() * defaults.pixel),
            width :  Math.round($('section').width() * defaults.pixel)
        }
        var products = {};
        function preGizle(){
            $(".text").removeClass('hide')
            $(".pre").addClass('hide')
        }
        function preGoster(){
            $(".text").addClass('hide')
            $(".pre").removeClass('hide')
        }
        function pageSetupOpt(){
            $(".save").on('click',function(){
                var height = $('input[name=hght]').val();
                var  width = $('input[name=hght]').val();
                section.width = width;
                section.height = height;

                $("section").attr('size','Custom');
                $("section").attr('layout','Custom');
                $(".pageScape").addClass('disabled');
                console.log
                $("section").css({width:width+'cm',height:height+'cm'});
                $("section").css("background-size", width+'cm '+height+'cm, cover');

                $("#myModal2").modal('hide');
            })
        }

        $('.boyut').on('click',function(){
            $(".boyut").removeClass('active')
            var id = $(this).data('id');
            $(this).addClass('active');

            if(id == 'custom'){
                $("#myModal2").find('.modal-title').html('Özel Sayfa Boyutu İçin Değer Giriniz');
                $("#myModal2").find('.modal-body').css('min-height','150px');
                $("#myModal2").find('.modal-body').html('<div class="col-md-6"><div class="form-group">' +
                        '<span><i class="fa fa-long-arrow-right"></i> Sayfa Eni</span>' +
                        '<input type="number" name="wdth" placeholder="CM" class="form-control" > ' +
                        '</div></div>'+'<div class="col-md-6"><div class="form-group">' +
                        '<span><i class="fa fa-long-arrow-down"></i> Sayfa Boyu</span>' +
                        '<input  type="number"  name="hght"   placeholder="CM" class="form-control " > ' +
                        '</div></div>'

                );
                $("#myModal2").find('.modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>'+
                        '<button type="button" class="btn save btn-primary">Ayarları Kaydet</button>')
                $("#myModal2").modal('show');
                pageSetupOpt()
                return ;
            }


            if(id != 'custom'){
                $(".pageScape").removeClass('disabled');
                $('section').css({width:'',height:''})
            }



           var layout = $('.layout .active').data('layout')
            $('section').attr('size',id);
            $('section').attr('layout',layout);
            preGizle()
        });

        $('.layout').on('click',function(){
            $(".layout").removeClass('active')
            var id = $(this).data('layout');

            $(this).addClass('active');
            var size = $('.boyut .active').data('id');


            $('section').attr('size',size);
            $('section').attr('layout',id);
            preGizle()
        });

        $(".context-menu__item").on('click',function(){
            var id = ($(this).data('id'));
            $(this).addClass('disabled');
            $('.saveArea').removeClass('hide');
            getOptions($(this),clickCoordsX,clickCoordsY);
            preGizle()
        });

        $(".image").on('click',function(){

            $("#myModal").modal('show');

            $("#imgInp").change(function(){
                readURL(this);
            });

        })
        $(".capture").on('click',function(){
            capture();
        })


        loadTemplate(url+'/callAPI',{},function(data){

            products = (data.invoice.products);


        })
        function capture() {

            var file_data = $('#imgInp').prop('files')[0];
            var form_data = new FormData();
            form_data.append('image', file_data);

            var datas = [];
            $.each($(".Designer-field"),function(index,value){
                var attr = $(this).data('attr');
                var height = $(this).height();
                var width = $(this).width();
                var left = $(this).data('x');
                var top = $(this).data('y');
                datas.push({attr:attr,height:height,width:width,left:left,top:top});

            });

            var json_arr = JSON.stringify(datas);
            var name = $("#inv_name").val();
            var size = $('section').attr('size');
            var layout = $('section').attr('layout');

            form_data.append('data', json_arr);
            form_data.append('invoice_name', name);
            form_data.append('size', size);
            form_data.append('layout', layout);
            form_data.append('invoice_id', '{{$data->id}}');
            form_data.append('customer_id', 1);
            form_data.append('width', Math.round($('section').width() * defaults.pixel));
            form_data.append('height', Math.round($('section').height() * defaults.pixel)  );


            $.ajax({
                url: url+'/capture',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    Messenger().post({
                        message: 'Kaydedildi',
                        type: 'success',
                        showCloseButton: true
                    });

                    preGoster();
                }
            });



        }

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $("section").css('background-image','url('+ e.target.result+')')
                    $("#myModal").modal('hide')
                }

                reader.readAsDataURL(input.files[0]);
            }
        }


        function loadTemplate(url,datas,callback){

            postData(url,datas,'POST','text',function(data){

                callback(data)
            })

        }
        Array.prototype.getUnique = function(){
            var u = {}, a = [];
            for(var i = 0, l = this.length; i < l; ++i){
                if(u.hasOwnProperty(this[i])) {
                    continue;
                }
                a.push(this[i]);
                u[this[i]] = 1;
            }
            return a;
        }
        function dragDrop(){
            interact('.Designer-field')
                    .draggable({
                        onmove: window.dragMoveListener,
                        onend:window.endListener,
                    })
                    .resizable({
                        preserveAspectRatio: false,
                        edges: {left: false, right: true, bottom: true, top: false},
                        onend:window.endListener,
                    })
                    .on('resizemove', function (event) {
                        var target = event.target,
                                x = (parseFloat(target.getAttribute('data-x')) || 0),
                                y = (parseFloat(target.getAttribute('data-y')) || 0);

                        // update the element's style
                        target.style.width = event.rect.width + 'px';
                        target.style.height = event.rect.height + 'px';

                        // translate when resizing from top or left edges
                        x += event.deltaRect.left;


                        target.style.webkitTransform = target.style.transform =
                                'translate(' + x + 'px,' + y + 'px)';

                        target.setAttribute('data-x', x);
                        target.setAttribute('data-y', y);
                        preGizle()
                        //target.textContent = Math.round(event.rect.width) + '×' + Math.round(event.rect.height);
                    });

            $('.Designer-field').on('click', '.fa-times', function () {

                var id = $(this).attr('data-ember-action');

                $("#ember" + id).remove();
                $("html").css('cursor','default');
                $("#features").addClass('hide')
                $("#features").addClass('closed')
                $('.context-menu__item[data-id='+id+']').removeClass('disabled');
                preGizle()
            })
        }
        function  getTable(id,txt,atr,width,height,left,top){

            $('#myModal3').modal('show');

            $(".saveChanges").on('click',function(){
                $("#myModal3").modal('hide');

            });



            $('#myModal3').on('hidden.bs.modal', function (event) {


                var checkedValues = $("#sortable2 li").map(function () {
                    return $(this).data('value');
                }).get();


                var elements = [];
                var cols = [];

                // console.log(products);
                $.each(products,function(index,value){
                    //console.log(value);
                    var stock_code = value.product.stock_code

                    var name = value.name;
                    var amount = value.amount;
                    var price = value.price;
                    var tax = value.tax;
                    var total = value.total;
                    var unit_name = value.unit_name;
                    var oiv = value.oiv;
                    var otv = value.otv;


                    var array = ['stock_code','name','amount','price','tax','total','unit_name','oiv','otv'];
                    var keys = [stock_code,name,amount,price,tax,total,unit_name,oiv,otv];



                    $.each(checkedValues,function(index,value){
                        var a = array.indexOf(value);
                        elements.push(keys[a]);
                        cols.push(value)
                    })






                });


                loadTemplate(url+'/addTable',{id:id,text:txt,attr:atr,w:width,h:height,l:left,t:top,cols:cols.getUnique(),elements:elements},function(data){
                    $('section').append(data);
                    $(".table ").css('border-color','black');
                    $(".last").css('height','200px');
                    $(".row").css('display','table-row');
                    $(".table td").css('border-color','black');
                    $(".table th").css('border-color','black');
                    $(".table tr").css('border-color','black');
                    $(".table thead").css('border-color','black');
                    $(".table tbody").css('border-color','black');
                });



            })


        }
        function getOptions(selector,x,y){
            var txt = selector.data('text');
            var atr = selector.data('attr');
            var width = selector.data('width');
            var height = selector.data('height');
            var id = selector.data('id');
            var left = x - 270;
            var top = y-100;


            loadTemplate(url+'/addItem',{id:id,text:txt,attr:atr,w:width,h:height,l:left,t:top},function(data) {

                if(atr == 'hizmet'){

                    getTable(id,txt,atr,width,height,left,top)

                } else {
                    $('section').append(data);
                }


                dragDrop();
            });


        }


        function endListener(event){
            var target = event.target;
            var object = {
                id:target.getAttribute('data-id'),
                x:target.getAttribute('data-x'),
                y:target.getAttribute('data-y'),
                w:target.offsetWidth,
                h:target.offsetHeight,
            }

            preGizle()
        }
        function dragMoveListener(event) {



            var target = event.target,

            // keep the dragged position in the data-x/data-y attributes
                    x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                    y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

            // translate the element
            target.style.webkitTransform =
                    target.style.transform =
                            'translate(' + x + 'px, ' + y + 'px)';

            // update the posiion attributes
            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);
            preGizle()
        }

        // this is used later in the resizing and gesture demos
        window.dragMoveListener = dragMoveListener;

        dragDrop();
        $.each($('.Designer-field'),function(index,value){

            var attr = $(this).attr('data-attr');
            $('.context-menu__item[data-attr='+attr+']').addClass('disabled');
        })


        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //
        // H E L P E R    F U N C T I O N S
        //
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////

        /**
         * Function to check if we clicked inside an element with a particular class
         * name.
         *
         * @param {Object} e The event
         * @param {String} className The class name to check against
         * @return {Boolean}
         */
        function clickInsideElement( e, className ) {
            var el = e.srcElement || e.target;

            if ( el.classList.contains(className) ) {
                return el;
            } else {
                while ( el = el.parentNode ) {
                    if ( el.classList && el.classList.contains(className) ) {
                        return el;
                    }
                }
            }

            return false;
        }

        /**
         * Get's exact position of event.
         *
         * @param {Object} e The event passed in
         * @return {Object} Returns the x and y position
         */
        function getPosition(e) {
            var posx = 0;
            var posy = 0;

            if (!e) var e = window.event;

            if (e.pageX || e.pageY) {
                posx = e.pageX;
                posy = e.pageY;
            } else if (e.clientX || e.clientY) {
                posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
            }

            return {
                x: posx,
                y: posy
            }
        }

        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //
        // C O R E    F U N C T I O N S
        //
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////



        /**
         * Initialise our application's code.
         */
        function init() {
            contextListener();
            clickListener();
            keyupListener();
            resizeListener();
        }

        /**
         * Listens for contextmenu events.
         */
        function contextListener() {
            document.addEventListener( "contextmenu", function(e) {
                taskItemInContext = clickInsideElement( e, taskItemClassName );

                if ( taskItemInContext ) {
                    e.preventDefault();
                    toggleMenuOn();
                    positionMenu(e);
                } else {
                    taskItemInContext = null;
                    toggleMenuOff();
                }
            });
        }

        /**
         * Listens for click events.
         */
        function clickListener() {
            document.addEventListener( "click", function(e) {
                var clickeElIsLink = clickInsideElement( e, contextMenuLinkClassName );

                if ( clickeElIsLink ) {
                    e.preventDefault();
                    menuItemListener( clickeElIsLink );
                } else {
                    var button = e.which || e.button;
                    if ( button === 1 ) {
                        toggleMenuOff();
                    }
                }
            });
        }

        /**
         * Listens for keyup events.
         */
        function keyupListener() {
            window.onkeyup = function(e) {
                if ( e.keyCode === 27 ) {
                    toggleMenuOff();
                }
            }
        }

        /**
         * Window resize event listener
         */
        function resizeListener() {
            window.onresize = function(e) {
                toggleMenuOff();
            };
        }

        /**
         * Turns the custom context menu on.
         */
        function toggleMenuOn() {
            if ( menuState !== 1 ) {
                menuState = 1;
                menu.classList.add( contextMenuActive );
            }
        }

        /**
         * Turns the custom context menu off.
         */
        function toggleMenuOff() {
            if ( menuState !== 0 ) {
                menuState = 0;
                menu.classList.remove( contextMenuActive );
            }
        }

        /**
         * Positions the menu properly.
         *
         * @param {Object} e The event
         */
        function positionMenu(e) {
            clickCoords = getPosition(e);
            clickCoordsX = clickCoords.x;
            clickCoordsY = clickCoords.y;

            menuWidth = menu.offsetWidth + 4;
            menuHeight = menu.offsetHeight + 4;

            windowWidth = window.innerWidth;
            windowHeight = window.height;





            if ( (windowWidth - clickCoordsX) < menuWidth ) {
                menu.style.left = windowWidth - menuWidth + "px";
            } else {
                menu.style.left = clickCoordsX + "px";
            }

            if ( (windowHeight - clickCoordsY) < menuHeight ) {
                menu.style.top = windowHeight - menuHeight + "px";
            } else {
                menu.style.top = clickCoordsY + "px";
            }
        }

        /**
         * Dummy action function that logs an action when a menu item link is clicked
         *
         * @param {HTMLElement} link The link that was clicked
         */
        function menuItemListener( link ) {
            //console.log( "Task ID - " + taskItemInContext.getAttribute("data-id") + ", Task action - " + link.getAttribute("data-action"));
           toggleMenuOff();

        }

        /**
         * Run the app.
         */
        init();

    })();
</script>
@endpush