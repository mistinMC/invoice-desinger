<!DOCTYPE html>
<html>
<head>
    <title>Fatura Düzenleme Sihirbazı</title>
    <!-- Bootstrap -->

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="/assets/css/all.css" rel="stylesheet">
    @stack('style')
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Nkolay Fatura</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="col-md-8">
                @yield('options')
            </div>

            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown pageScheme">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Şablonlarım <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Şablon 1</a></li>
                        <li><a href="#">Şablon 2</a></li>
                        <li><a href="#">Şablon 3</a></li>
                        <li role="separator" class="divider"></li>
                        <li class="dropdown-header">Yeni</li>
                        <li><a class="boyut active" data-id="A4" href="#">A4</a></li>
                        <li><a class="boyut" data-id="A5" href="#">A5</a></li>
                        <li><a class="boyut" data-id="custom" href="#">Özel</a></li>

                    </ul>
                </li>
                <li class="dropdown pageScape">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Sayfa Boyutu <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a class="layout active" data-layout="portrait" href="#">Dikey</a></li>
                        <li><a class="layout" data-layout="landscape" href="#">Yatay</a></li>
                    </ul>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>

@yield('content')


        <!-- Modal -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Fatura Şablonu Kılavuz Fatura Tarama İmajı Yükleme</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <span><i class="fa fa-long-arrow-right"></i> Sayfa Eni</span>
                        <input type="number" name="wdth" placeholder="CM" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <span><i class="fa fa-long-arrow-down"></i> Sayfa Boyu</span>
                        <input type="number" name="hght" placeholder="CM" class="form-control ">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Fatura Şablonu Kılavuz Fatura Tarama İmajı Yükleme</h4>
            </div>
            <div class="modal-body">
                <form id="form1" runat="server" enctype="multipart/form-data">
                    <input type='file' id="imgInp"/>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>

            </div>
        </div>
    </div>
</div>
<script src="/assets/js/all.js"></script>
<script src="/assets/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/js/prettify.js"></script>
@stack('scripts')
</body>
</html>