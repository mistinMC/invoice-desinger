<?php


Route::get('/', 'invInvoiceController@index');
Route::get('invoice', 'invInvoiceController@index');
Route::get('invoice/detail/{id}', 'invInvoiceController@detail');
Route::get('invoice/preview/{id}', 'invInvoiceController@preview');
Route::get('invoice/preview/{id}/{fatura_id}', 'invInvoiceController@preview');
Route::get('invoice/print/{id}/{fatura_id}', 'invInvoiceController@print');
Route::group(['prefix'=>'invapi'],function(){

    Route::any('/{method}','invInvoiceController@api');

});