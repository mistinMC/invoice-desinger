/**
 * Created by mc on 28.07.2016.
 */
function postData(url,formData,method,type,callback){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'INVOICE-ID': $('meta[name="invoice"]').attr('content'),

        }
    });
    $.ajax({
        url:url,
        type: method,
        data: formData,
        async: false,
        success: function (data) {
            var types = 'notify';
            if(type){
               types = type;
            }
            if (data.error) {
                getError(data.error, types);
                return false;
            }
            if (data.errors) {
                 getErrors(data.errors, types);
                return false;
            }
            callback(data);

        }

    });

    return false;

}
function returnData(data){

    if(data.redirect){
        return window.location.href=data.url;
    }


    return data;
}

function getErrors(errors,type){

    if(type == 'text'){
        var text = '';
        $.each(errors, function( index, value ) {
            text+=('<li>'+value+'</li>');
        });
        $('ul.errors').html(text);
        return false;
    }

    $.each(errors, function( index, value ) {
        //notify('fa fa-exclamation-circle','Error: '+index+' ',value,'danger');
    });

}
function getError(error,type){

    if(type == 'text'){
       var text = error;
        $('ul.errors').html(text);
        return false;
    }

    //notify('fa fa-exclamation-circle','Error: 1 ',error,'danger');

}
